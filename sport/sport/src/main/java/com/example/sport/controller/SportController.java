package com.example.sport.controller;
import com.example.sport.exception.SportAlreadyExists;
import com.example.sport.exception.SportNotFound;
import com.example.sport.model.Sport;
import com.example.sport.model.Team;
import com.example.sport.service.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/sportApp/v")
public class SportController {

    private SportService sportService;

    @Autowired
    public SportController(SportService sportService){
        this.sportService=sportService;
    }

    //http://localhost:7000/sportApp/v/getSport
    @GetMapping("/getSport")
    public ResponseEntity<?> getSport() {
        return new ResponseEntity<>(sportService.getSport(), HttpStatus.OK);
    }

    //http://localhost:7000/sportApp/v/addSport
    @PostMapping("/addSport")
    public ResponseEntity<?> addSport(@RequestBody Sport sport) throws SportAlreadyExists {
        try {
            return new ResponseEntity<>(sportService.addSport(sport), HttpStatus.OK);
        } catch (SportAlreadyExists exception) {
            throw new SportAlreadyExists();
        }
    }

    //http://localhost:7000/sportApp/v/createTeam/{spoId}
    @PostMapping("/createTeam/{spoId}")
    public ResponseEntity<?> createTeam(@RequestBody Team team,@PathVariable int spoId) throws SportNotFound {
        return new ResponseEntity<>(sportService.createTeam(team, spoId),HttpStatus.OK);
    }

    //http://localhost:7000/sportApp/v/getTeam/{spoId}
    @GetMapping("/getTeam/{spoId}")
    public ResponseEntity<?> getTeam(@PathVariable int spoId) throws SportNotFound{
        List<Team>teamList=sportService.getAllTeam(spoId);
        return new ResponseEntity<>(teamList,HttpStatus.OK);
    }

    //http://localhost:7000/sportApp/v/teamByCity/{city}
    @GetMapping("/teamByCity/{city}")
    public ResponseEntity<?> teamByCity(@PathVariable String city) throws SportNotFound{
        return new ResponseEntity<>(sportService.getTeamByCity(city),HttpStatus.OK);
    }


}
