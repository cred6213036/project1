package com.example.sport.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT,reason = "Sports already exists")
public class SportAlreadyExists extends Exception {
}


