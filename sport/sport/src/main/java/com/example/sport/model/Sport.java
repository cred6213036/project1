package com.example.sport.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
@Document

public class Sport {

    @Id
    private Integer spoId;
    private String sportName;
    private List<Team> teamList;
    public Sport() {
        teamList=new ArrayList<Team>();
    }
}
