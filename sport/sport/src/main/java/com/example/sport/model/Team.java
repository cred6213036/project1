package com.example.sport.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class Team {

    @Id
    private String teamName;
    private String city ;
    private int noOfPlayer;
}
