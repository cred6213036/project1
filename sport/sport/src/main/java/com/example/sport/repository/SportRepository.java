package com.example.sport.repository;
import com.example.sport.model.Sport;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import java.util.List;

public interface SportRepository extends MongoRepository<Sport,Integer> {

    @Query("{'teamList.city' :{$in:[?0]}}")
    public abstract List<Sport> findByCity(String city);

}
