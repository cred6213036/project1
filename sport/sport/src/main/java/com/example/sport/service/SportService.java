package com.example.sport.service;
import com.example.sport.exception.SportAlreadyExists;
import com.example.sport.exception.SportNotFound;
import com.example.sport.model.Sport;
import com.example.sport.model.Team;
import java.util.List;

public interface SportService {

    public abstract List<Sport> getSport();
    public abstract Sport addSport(Sport sport) throws SportAlreadyExists;
    boolean createTeam(Team team,int spoId) throws SportNotFound;
    List<Team> getAllTeam(int spoId) throws SportNotFound;
    public abstract List<Sport> getTeamByCity(String city) throws SportNotFound;

}
