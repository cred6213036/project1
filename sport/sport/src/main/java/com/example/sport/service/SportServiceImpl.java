package com.example.sport.service;
import com.example.sport.exception.SportAlreadyExists;
import com.example.sport.exception.SportNotFound;
import com.example.sport.model.Sport;
import com.example.sport.model.Team;
import com.example.sport.repository.SportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class SportServiceImpl implements SportService {

    @Autowired
    SportRepository sportRepository;


    @Override
    public List<Sport> getSport() {
        return sportRepository.findAll();
    }

    @Override
    public Sport addSport(Sport sport) throws SportAlreadyExists {
        if (sportRepository.findById(sport.getSpoId()).isPresent()){
            throw new SportAlreadyExists();
        }
        else {
            return sportRepository.insert(sport);
        }
    }

    @Override
    public boolean createTeam(Team team, int spoId) throws SportNotFound {
        if (sportRepository.findById(spoId).isEmpty()){
            throw new SportNotFound();
        }
        Sport sport=sportRepository.findById(spoId).get();
        List<Team> teams=sport.getTeamList();
        if (teams==null){
            teams=new ArrayList<>();
        }
        teams.add(team);
        sport.setTeamList(teams);
        sportRepository.save(sport);
        return true;
    }

    @Override
    public List<Team> getAllTeam(int spoId) throws SportNotFound {
        if (sportRepository.findById(spoId).get()==null){
            throw new SportNotFound();
        }
        Sport sport=sportRepository.findById(spoId).get();
        List<Team> teams=sport.getTeamList();
        return teams;
    }

    @Override
    public List<Sport> getTeamByCity(String city) throws SportNotFound {
        return  sportRepository.findByCity(city);
    }

}
